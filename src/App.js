import { Fragment, useRef, useState } from "react";
import "./App.css";

function App() {
  const [playing, setPlaying] = useState(false);
  const [videoTime, setVideoTime] = useState(0);
  const [currentTime, setCurrentTime] = useState(0);
  const [progress, setProgress] = useState(0);
  const [showControls, setShowControls] = useState(true);

  const videoRef = useRef(null);

  const videoHandler = (control) => {
    if (control === "play") {
      videoRef.current.play();
      setPlaying(true);
      const vid = document.getElementById("video1");
      setVideoTime(vid.duration);
    } else if (control === "pause") {
      videoRef.current.pause();
      setPlaying(false);
    }
  };

  const fastForward = () => {
    videoRef.current.currentTime += 5;
  };

  const revert = () => {
    videoRef.current.currentTime -= 5;
  };

  setInterval(() => {
    setCurrentTime(videoRef.current?.currentTime);
    setProgress((videoRef.current?.currentTime / videoTime) * 100);
  }, 1000);

  return (
    <div className="App">
      <div
        className="video-container"
        onMouseOver={() => setShowControls(true)}
        onMouseLeave={() => setShowControls(false)}
      >
        <video
          id="video1"
          ref={videoRef}
          className="video"
          src="https://res.cloudinary.com/dssvrf9oz/video/upload/v1635662987/pexels-pavel-danilyuk-5359634_1_gmixla.mp4"
        />
        {showControls && (
          <Fragment>
            <div className="controls">
              <img
                onClick={revert}
                className="controlsIcon"
                alt=""
                src="/assets/backward.png"
              />
              {playing ? (
                <img
                  onClick={() => videoHandler("pause")}
                  className="controlsIcon--small"
                  alt=""
                  src="/assets/pause.png"
                />
              ) : (
                <img
                  onClick={() => videoHandler("play")}
                  className="controlsIcon--small"
                  alt=""
                  src="/assets/play.png"
                />
              )}
              <img
                onClick={fastForward}
                className="controlsIcon"
                alt=""
                src="/assets/forward.png"
              />
            </div>
            <div className="timecontrols">
              <p className="controlsTime">
                {Math.floor(currentTime / 60) +
                  ":" +
                  ("0" + Math.floor(currentTime % 60)).slice(-2)}
              </p>
              <div className="time_progressbarContainer">
                <div
                  style={{ width: `${progress}%` }}
                  className="time_progressBar"
                ></div>
              </div>
              <p className="controlsTime">
                {Math.floor(videoTime / 60) +
                  ":" +
                  ("0" + Math.floor(videoTime % 60)).slice(-2)}
              </p>
            </div>
          </Fragment>
        )}
      </div>
    </div>
  );
}

export default App;
